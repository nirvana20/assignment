import os, sys

## CERT_DIR is not currently needed
# CERT_DIR = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "certs")
# print(CERT_DIR)
if os.getenv('SETTINGS_MODE', '') == 'prod':
    # Running in production, definitely want to access the Google Cloud SQL instance
    # in production.
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'pharmacypro',
            'USER': 'pharmacyprod',
            'PASSWORD': '3l3NZ,=9i[',
            'HOST': '104.155.223.31',
        }
    }
elif os.getenv('SETTINGS_MODE') == 'local':
    # Running in development, need access to local mysql
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'test',
            'USER': 'test',
            'PASSWORD': 'test',
            'HOST': '127.0.0.1',
        }
    }
elif 'test' in sys.argv:
    PROJECT_DIR = os.path.abspath(os.path.dirname(__file__))
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(PROJECT_DIR, 'db.sqlite3'),
        }
    }
else:
    # Running in development, so use a local MySQL database.
    PROJECT_DIR = os.path.abspath(os.path.dirname(__file__))
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(PROJECT_DIR, 'db.sqlite3'),
        }
    }
