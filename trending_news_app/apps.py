from django.apps import AppConfig


class TrendingNewsAppConfig(AppConfig):
    name = 'trending_news_app'
