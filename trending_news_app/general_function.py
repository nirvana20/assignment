from .models import Article, Country, Category
from datetime import datetime
from django.core.cache import cache


def hit_api_check(country, category):
    news_api_hit_check = False
    if country + '_' + category not in cache:
        cache.set(country + '_' + category, datetime.now())
        news_api_hit_check = True
    else:
        last_time = cache.get(country + '_' + category)
        current_time = datetime.now()
        time_diffrence = current_time - last_time
        minute_elasped = divmod(time_diffrence.days * 86400 + time_diffrence.seconds, 60)[0]
        if minute_elasped >= 10:
            news_api_hit_check = True
    return news_api_hit_check


def save_article_obj(json_data, category, country):
    for data in json_data['article']:
        category_obj, created_category = Category.objects.get_or_create(category_name=category)
        country_obj, created_country = Country.objects.get_or_create(country_name=country)
        article_lst = []
        if not Article.objects.filter(url_link=(data['url_link'])).exists() and len(data['url_link']) < 255:
            article_obj = Article(**data)
            article_obj.category = category_obj
            article_obj.country = country_obj
            article_lst.append(article_obj)
        Article.objects.bulk_create(article_lst)
    return 'ok'