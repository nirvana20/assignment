from django.db import models
from django.utils import timezone


class Country(models.Model):
    country_name = models.TextField(null=False,blank=False)


class Category(models.Model):
    category_name = models.TextField(null=False, blank=False)


class Article(models.Model):

    url_link = models.CharField(null=False, blank=False, unique=True, max_length=255)
    publish_date = models.DateTimeField(blank=True, null=True)
    title = models.TextField(null=True, blank=True)
    author = models.CharField(max_length=256, null=True,blank=False)
    content = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    source_id = models.TextField(null=True, blank=True)
    source_name = models.TextField(null=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.DO_NOTHING, null=True, blank=True)
    country = models.ForeignKey(Country, on_delete=models.DO_NOTHING, null=True,blank=True)
    created_at = models.DateTimeField(editable=False)

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        return super(Article, self).save(*args, **kwargs)

    class Meta:
        ordering = ['-created_at']