from rest_framework import serializers
from trending_news_app.models import *


class CountrySerializers(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ('country_name',)


class CategorySerializers(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('category_name',)


class ArticleSerializers(serializers.ModelSerializer):
    country = CountrySerializers()
    category = CategorySerializers()

    class Meta:
        model = Article
        fields = '__all__'

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret['category'] = ret['category']['category_name']
        ret['country'] = ret['country']['country_name']
        return ret