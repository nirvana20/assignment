from trending_news_app.config import API_KEY, API_URL
import requests
import datetime


def fetchdatanewsapi(country, category):
    api_url = API_URL + API_KEY + "&country=" + country + "&category=" + category
    resp = requests.get(api_url)
    lst = []
    article_list = resp.json()
    if resp.status_code == 200:
        article_list = resp.json()['articles']
        for data in article_list:
            source_id = data['source']['id']
            data['source'] = data['source']['name']
            data['source_name'] = data.pop('source')
            data['source_id'] = source_id
            data['url_link'] = data.pop('url')
            data['publish_date'] = data.pop('publishedAt')
            del data['urlToImage']
            data['created_at'] = datetime.datetime.now()
        lst.append({'status': 'ok', 'article':  article_list})
        return({'status': 'ok', 'article':  article_list})
    else:
        # This means something went wrong.
        return article_list
