from unittest.mock import patch, Mock
from django.test import TestCase
from rest_framework.test import APIRequestFactory
from .models import Article,Category,Country
from .serializers import *
from .views import ArticleViewset
import unittest
from .general_function import *

service_response = {
  "status": "ok",
  "totalResults": 20,
  "articles": [
    {
      "source": {
        "id": "nfl-news",
        "name": "NFL News"
      },
      "author": "Around The NFL staff",
      "title": "Injuries: Davenport expected to miss about a month",
      "description": "Saints defensive end Marcus Davenport suffered a toe injury in Week 8 and is undergoing tests, NFL Network Insider Ian Rapoport reported. Here are other injuries we're tracking.",
      "url": "http://www.nfl.com/news/story/0ap3000000981730/article/injuries-davenport-expected-to-miss-about-a-month",
      "urlToImage": "http://static.nfl.com/static/content/public/photo/2018/10/31/0ap3000000981752_thumbnail_200_150.jpg",
      "publishedAt": "2018-10-31T17:10:42Z",
      "content": "The New Orleans Saints suddenly have concerns on the defensive line ahead of Sunday's showdown against the Los Angeles Rams. Rookie defensive end Marcus Davenport is expected to miss about a month with a toe inury, sources told NFL Network Insider Ian Rapopor… [+1575 chars]"
    }
    ]
}

article = {
            "url_link": "https://testing.com",
            "publish_date": "2018-10-31T15:20:48Z",
            "title": "testing",
            "author": "sumeet",
            "content": "this is the content  to test our api ",
            "description": "testing our viewset",
            "source_id": 'testing',
            "source_name": "testing",
            }
country = {"country_name": "in"}
category = {"category_name": "sport"}

article_for_save = {'article': [{
            "url_link": "https://testing.com",
            "publish_date": "2018-10-31T15:20:48Z",
            "title": "testing",
            "author": "sumeet",
            "content": "this is the content  to test our api ",
            "description": "testing our viewset",
            "source_id": 'testing',
            "source_name": "testing",
            }]}
test_save_country = 'us'
test_save_category = 'sport'

class TestNewsApp(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()

        self.article_obj = Article.objects.create(**article)
        self.country_obj = Country.objects.create(**country)
        self.category_obj = Category.objects.create(**category)
        self.article_obj.category = self.category_obj
        self.article_obj.country = self.country_obj
        self.article_obj.save()

    def test_get_article(self):
        from trending_news_app.views import ArticleViewset
        request = self.factory.get('/news/trending/')
        response = ArticleViewset.as_view({'get': 'list'})(request)
        self.assertEqual(response.status_code, 400)
        request = self.factory.get('/news/trending/', {'country': 'in'})
        response = ArticleViewset.as_view({'get': 'list'})(request)
        self.assertEqual(response.status_code, 400)
        request = self.factory.get('/news/trending/', {'country': 'in', 'category' :'sport'})
        response = ArticleViewset.as_view({'get': 'list'})(request)
        self.assertEqual(response.status_code, 400)
        request = self.factory.get('/news/trending/', {'country': 'in', 'category': 'sport','q': 'testing'})
        response = ArticleViewset.as_view({'get': 'list'})(request)
        self.assertEqual(response.status_code, 200)

    @patch('trending_news_app.service.requests.get')
    def test_trending_news_app(self,mock_get):
        mock_get.return_value = Mock(ok=True)
        mock_get.return_value.json.return_value = service_response
        from trending_news_app.general_function import save_article_obj
        res = save_article_obj(article_for_save, test_save_category, test_save_country)
        self.assertEqual(res, 'ok')

    def test_hit_api_check(self):
        from django.core.cache import cache
        from trending_news_app.general_function import hit_api_check
        cache.set(test_save_country + '_' + test_save_category, datetime.now())
        res = hit_api_check(test_save_country, test_save_category)
        self.assertEqual(res, False)
        cache.delete(test_save_country + '_' + test_save_category)
        res = hit_api_check(test_save_country, test_save_category)
        self.assertEqual(res, True)

    def test_service_function(self):
        from trending_news_app.service import fetchdatanewsapi
        res = fetchdatanewsapi('in', 'sport')
        self.assertEqual(res['status'], 'ok')