from rest_framework.routers import SimpleRouter
from . import views

from django.conf.urls import url

router = SimpleRouter()
router.register(r'trending', views.ArticleViewset, base_name=r'trending')
# router.register(r'ingredient_details', views.IngredientViewset, base_name=r'ingredient_details')
# router.register(r'recipe_details', views.RecipeViewset, base_name=r'recipe_details')
# router.register(r'user_food_mapping', views.UserFoodMappingViewset, base_name=r'user_food_mapping')
# router.register('myidealfood', viewset=views.UserFoodLoggingNutritionModelViewset, base_name='myidealfood')
urlpatterns = router.urls