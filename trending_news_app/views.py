from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from rest_framework.renderers import JSONRenderer
from rest_framework import parsers
from .serializers import ArticleSerializers
from .models import *
from rest_framework.viewsets import ReadOnlyModelViewSet, GenericViewSet
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.response import Response
from django.core.cache import cache
from .service import fetchdatanewsapi
from .general_function import *
from rest_framework.exceptions import ValidationError
from datetime import datetime
from django.db.models import Q


class ArticleViewset(ReadOnlyModelViewSet, GenericViewSet):
    """

    list: return a list of Article matches q (search_query) Please set the following parameters and try again:q, country, category."

    retrieve: return a particular Article data
    """
    parser_classes = (parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser)
    renderer_classes = (JSONRenderer,)
    serializer_class = ArticleSerializers
    queryset = Article.objects.all()

    def list(self, request, *args, **kwargs):
        country = request.query_params.get('country')
        category = request.query_params.get('category')
        search_query = request.query_params.get('q')

        if not search_query or not country or not category:
            raise ValidationError({"status": "error", "code": "parametersMissing", "message": "Required parameters are missing. Please set the following parameters and try again:q, country, category."})
        if hit_api_check(country, category):
            json_data = fetchdatanewsapi(country, category)
            if json_data['status'] == 'error':
                raise ValidationError(json_data)
            save_article_obj(json_data, category, country)
        queryset = Article.objects.filter(Q(category__category_name=category), Q(country__country_name=country), Q(content__icontains=search_query) | Q(description__icontains=search_query) )
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)

        return Response(serializer.data)